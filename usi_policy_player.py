﻿import usi, policy_player, argparse

argparser = argparse.ArgumentParser()
argparser.add_argument("--model", "-m", type=str, default=r'model\model_policy')
args = argparser.parse_args()

player = policy_player.PolicyPlayer(args.model)
usi.usi(player)
