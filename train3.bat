@echo off
python train_resnet.py data/kifu_2017_10000_train.txt data/kifu_2017_10000_test.txt -e 10 -m resnet_block10_batch16 -bl 10 -rl 0.1 -b 16
python train_resnet.py data/kifu_2017_10000_train.txt data/kifu_2017_10000_test.txt -e 10 -m resnet_block10_batch16 -bl 10 -rl 0.1 -mi model/model_resnet_block10 -si model/state_resnet_block10
python train_resnet.py data/kifu_2017_10000_train.txt data/kifu_2017_10000_test.txt -e 10 -m resnet_block10 -bl 10 -rl 0.01  -mi model/model_resnet_block10 -si model/state_resnet_block10
python train_resnet.py data/kifu_2017_10000_train.txt data/kifu_2017_10000_test.txt -e 10 -m resnet_block10 -bl 10 -rl 0.001  -mi model/model_resnet_block10 -si model/state_resnet_block10

