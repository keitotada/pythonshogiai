from chainer import cuda, Variable, optimizers, serializers
import chainer.functions as funcs

from datetime import datetime
import argparse, random, pickle, os, re, sys, numpy as np, shutil

from src import features as feat, read_kifu
from src.network import policy


def trainer(data_train, data_test, model_init, resume, epoch, batchsize, batchsize_test, path_model, rate_learning, internal_eval):

    progress = 0
    time_start = datetime.now()
    time_end = datetime.now()

    def log(mes):
        sys.stdout.write("\r{} {}\nProgress: {}%, End: {}".format(datetime.now().strftime("%Y/%m/%d/%M:%S"), mes, progress, "unknown" if time_end == time_start else time_end.strftime("%Y/%m/%d/%M:%S")))
        sys.stdout.flush()

    def log_file(dir_log, mes):
        log(mes)
        with open(dir_log + '/log.txt', 'a') as _file:
            _file.write(mes + '\n')

    date = 'model/history/' + datetime.now().strftime('%Y%m%d%H%M%S')
    os.makedirs(date)

    model = policy.Network()
    gpu_device = 0
    cuda.get_device(gpu_device).use()
    model.to_gpu(gpu_device)

    optimizer = optimizers.SGD(lr=rate_learning)
    optimizer.setup(model)

    if model_init:
        log_file(date, 'Load model from {}'.format(model_init))
        serializers.load_npz(model_init, model)

    if resume:
        log_file(date, 'Load optimizer state from {}'.format(resume))
        serializers.load_npz(resume, optimizer)

    log('Start reading kifu')

    pic_train = re.sub(r'\..*?$', '', data_train) + '.pickle'
    if os.path.exists(pic_train):

        with open(pic_train, 'rb') as _file:
            positions_train = pickle.load(_file)
        log('Loaded pickle of training')

    else:

        log('Save pickle of training')
        positions_train = read_kifu.read_kifu(data_train)
        with open(pic_train, 'wb') as _file:
            pickle.dump(positions_train, _file, pickle.HIGHEST_PROTOCOL)
        log('Saved pickle of training')

    pic_test = re.sub(r'\..*?$', '', data_test) + '.pickle'
    if os.path.exists(pic_test):

        with open(pic_test, 'rb') as _file:
            positions_test = pickle.load(_file)
        log('Loaded pickle of testing')

    else:

        positions_test = read_kifu.read_kifu(data_test)
        with open(pic_test, 'wb') as _file:
            pickle.dump(positions_test, _file, pickle.HIGHEST_PROTOCOL)
        log('Saved pickle of testing')

    log('Finished reading kifu')



    log_file(date, 'train position num = {}'.format(len(positions_train)))
    log_file(date, 'test position num = {}'.format(len(positions_test)))

    def mini_batch(_positions, i, _batchsize):
        mini_batch_data = []
        mini_batch_move = []
        for b in range(_batchsize):
            features, move, win = feat.make_features(_positions[i + b])
            mini_batch_data.append(features)
            mini_batch_move.append(move)

        return (Variable(cuda.to_gpu(np.array(mini_batch_data, dtype=np.float32))),
                Variable(cuda.to_gpu(np.array(mini_batch_move, dtype=np.int32))))

    def mini_batch_for_test(_positions, _batchsize):
        mini_batch_data = []
        mini_batch_move = []
        for b in range(_batchsize):
            features, move, win = feat.make_features(random.choice(_positions))
            mini_batch_data.append(features)
            mini_batch_move.append(move)

        return (Variable(cuda.to_gpu(np.array(mini_batch_data, dtype=np.float32))),
                Variable(cuda.to_gpu(np.array(mini_batch_move, dtype=np.int32))))

    log('Start training')
    itr = 0
    sum_loss = 0
    for e in range(epoch):

        positions_train_shuffled = random.sample(positions_train, len(positions_train))
        itr_epoch = 0
        sum_loss_epoch = 0
        for i in range(0, len(positions_train_shuffled) - batchsize, batchsize):

            x, t = mini_batch(positions_train_shuffled, i, batchsize)
            y = model(x)

            model.cleargrads()
            loss = funcs.softmax_cross_entropy(y, t)
            loss.backward()
            optimizer.update()

            itr += 1
            sum_loss += loss.data
            itr_epoch += 1
            sum_loss_epoch += loss.data


            if optimizer.t % internal_eval == 0:
                x, t = mini_batch_for_test(positions_test, batchsize_test)
                y = model(x)

                log_file(date, 'epoch: {}, iter: {}, loss: {}, accur: {}'.format(optimizer.epoch + 1, optimizer.t, sum_loss / itr, funcs.accuracy(y, t).data))
                itr = 0
                sum_loss = 0




        log('Validate test data')
        itr_test = 0
        sum_test_accuracy = 0
        for i in range(0, len(positions_test) - batchsize, batchsize):

            x, t = mini_batch(positions_test, i, batchsize)
            y = model(x)
            itr_test += 1
            sum_test_accuracy += funcs.accuracy(y, t).data

        log_file(date, 'epoch: {}, iter: {}, loss: {}, accur: {}'.format(optimizer.epoch + 1, optimizer.t, sum_loss_epoch / itr_epoch, sum_test_accuracy / itr_test))

        progress = (e + 1) / epoch * 100
        time_end = (datetime.now() - time_start) / (e + 1) * (epoch - e - 1) + datetime.now()


        optimizer.new_epoch()


    serializers.save_npz(path_model + 'model_policy', model)

    shutil.copy2(path_model + 'model_policy', date + '/model_policy')
    log('Saved the model!')

    serializers.save_npz(path_model + 'state_policy', optimizer)
    shutil.copy2(path_model + 'state_policy', date + '/state_policy')
    log('Saved the optimizer!')

    log_file(date, '{} was required'.format((datetime.now() - time_start)))


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('data_train', type=str)
    parser.add_argument('data_test', type=str)
    parser.add_argument('--model_init', '-mi', default='')
    parser.add_argument('--resume', '-r', default='')
    parser.add_argument('--epoch', '-e', type=int, default=1)
    parser.add_argument('--batchsize', '-b', type=int, default=32)
    parser.add_argument('--batchsize_test', '-bt', type=int, default=512)
    parser.add_argument('--model', '-m', type=str, default='model/')
    parser.add_argument('--rate_learning', '-rl', type=float, default=0.01)
    parser.add_argument('--interval_eval', '-i', type=int, default=1000)
    args = parser.parse_args()

    trainer(args.data_train, args.data_test, args.model_init, args.resume, args.epoch, args.batchsize, args.batchsize_test, args.model, args.rate_learning, args.interval_eval)
