from chainer import cuda, Variable, optimizers, serializers, functions as funcs
import cupy as cp

from datetime import datetime
import argparse, random, pickle, os, re, sys, numpy as np, shutil

from src import features as fea, read_kifu
from src.network import densenet


def trainer(data_train, data_test, name_model, model_init, state_init, epoch, batchsize, batchsize_test, block, rate_learning, internal_eval):

    progress = 0
    time_start = datetime.now()
    time_end = datetime.now()

    date = 'model/history/' + datetime.now().strftime('%Y%m%d%H%M%S') + name_model
    os.makedirs(date)

    def log(mes: str, iswrite: bool, isconsole: bool):

        if iswrite:
            with open(date + '/log.txt', 'a') as _file:
                _file.write(mes + '\n')

        if isconsole:
            sys.stdout.write("\r{} {}\nProgress: {}%, End: {}".format(datetime.now().strftime("%Y/%m/%d/%M:%S"), mes, progress, "unknown" if time_end == time_start else time_end.strftime("%Y/%m/%d/%M:%S")))
        sys.stdout.flush()

    log(name_model, iswrite=True, isconsole=False)
    log('Training Data: ' + data_train, iswrite=True, isconsole=False)
    log('Testing Data: ' + data_test, iswrite=True, isconsole=False)
    log('Resnet Block: {}'.format(block), iswrite=True, isconsole=False)

    model = densenet.Network(block)
    gpu_device = 0
    cuda.get_device(gpu_device).use()
    model.to_gpu(gpu_device)

    optimizer = optimizers.SGD(lr=rate_learning)
    optimizer.setup(model)

    if model_init:
        log('Init Model: ' + model_init, iswrite=True, isconsole=True)
        serializers.load_npz(model_init, model)

    if state_init:
        log('Init Opt State' + state_init, iswrite=True, isconsole=True)
        serializers.load_npz(state_init, optimizer)

    log('Start reading kifu', iswrite=False, isconsole=True)

    pic_train = re.sub(r'\..*?$', '', data_train) + '.pickle'

    if os.path.exists(pic_train):

        log('Load pickle of training', iswrite=False, isconsole=True)
        with open(pic_train, 'rb') as _file:
            positions_train = pickle.load(_file)

    else:

        log('Save pickle of training', iswrite=False, isconsole=True)
        positions_train = read_kifu.read_kifu(data_train)
        with open(pic_train, 'wb') as _file:
            pickle.dump(positions_train, _file, pickle.HIGHEST_PROTOCOL)

    pic_test = re.sub(r'\..*?$', '', data_test) + '.pickle'
    if os.path.exists(pic_test):

        log('Load pickle of testing', iswrite=False, isconsole=True)
        with open(pic_test, 'rb') as _file:
            positions_test = pickle.load(_file)

    else:

        log('Save pickle of testing', iswrite=False, isconsole=True)
        positions_test = read_kifu.read_kifu(data_test)
        with open(pic_test, 'wb') as _file:
            pickle.dump(positions_test, _file, pickle.HIGHEST_PROTOCOL)

    log('Finished reading kifu', iswrite=False, isconsole=True)

    def mini_batch(_positions, i, _batchsize):
        mini_batch_data = []
        mini_batch_move = []
        mini_batch_win = []
        for b in range(_batchsize):
            feat, move, win = fea.make_features(_positions[i + b])
            mini_batch_data.append(feat)
            mini_batch_move.append(move)
            mini_batch_win.append(win)

        return (Variable(cp.array(mini_batch_data, dtype=cp.float32)),
                Variable(cp.array(mini_batch_move, dtype=cp.int32)),
                Variable(cp.array(mini_batch_win, dtype=cp.int32)).reshape(-1, 1))

    def mini_batch_for_test(_positions, _batchsize):
        mini_batch_data = []
        mini_batch_move = []
        mini_batch_win = []
        for b in range(_batchsize):
            feat, move, win = fea.make_features(random.choice(_positions))
            mini_batch_data.append(feat)
            mini_batch_move.append(move)
            mini_batch_win.append(win)

        return (Variable(cp.array(mini_batch_data, dtype=cp.float32)),
                Variable(cp.array(mini_batch_move, dtype=cp.int32)),
                Variable(cp.array(mini_batch_win, dtype=cp.int32)).reshape(-1, 1))

    log('Start training', iswrite=False, isconsole=True)
    itr = 0
    sum_loss = 0
    log('Epoch: {}'.format(epoch), iswrite=True, isconsole=True)
    log('Training Batchsize: {}'.format(batchsize), iswrite=True, isconsole=True)
    log('Testing Batchsize: {}'.format(batchsize_test), iswrite=True, isconsole=True)
    log('Rate of learning: {}'.format(rate_learning), iswrite=True, isconsole=True)
    log('Interval Eval: {}'.format(internal_eval), iswrite=True, isconsole=True)
    for e in range(epoch):

        positions_train_shuffled = random.sample(positions_train, len(positions_train))
        itr_epoch = 0
        sum_loss_epoch = 0
        for i in range(0, len(positions_train_shuffled) - batchsize, batchsize):

            x, t1, t2 = mini_batch(positions_train_shuffled, i, batchsize)
            y1, y2 = model(x)

            model.cleargrads()
            loss = funcs.softmax_cross_entropy(y1, t1) + funcs.sigmoid_cross_entropy(y2, t2)
            loss.backward()
            optimizer.update()

            itr += 1
            sum_loss += loss.data
            itr_epoch += 1
            sum_loss_epoch += loss.data

            if optimizer.t % internal_eval == 0:
                x, t1, t2 = mini_batch_for_test(positions_test, batchsize_test)
                y1, y2 = model(x)

                log('epoch: {}, iter: {}, loss: {}, accur: {}, {}'.format(optimizer.epoch + 1, optimizer.t, sum_loss / itr, funcs.accuracy(y1, t1).data, funcs.binary_accuracy(y2, t2).data), iswrite=True, isconsole=True)
                itr = 0
                sum_loss = 0

        log('Validate test data', iswrite=False, isconsole=True)
        itr_test = 0
        sum_test_accuracy1 = 0
        sum_test_accuracy2 = 0
        for i in range(0, len(positions_test) - batchsize, batchsize):

            x, t1, t2 = mini_batch(positions_test, i, batchsize)
            y1, y2 = model(x)
            itr_test += 1
            sum_test_accuracy1 += funcs.accuracy(y1, t1).data
            sum_test_accuracy2 += funcs.binary_accuracy(y2, t2).data

        log('epoch: {}, iter: {}, loss: {}, accur: {}, {}'.format(optimizer.epoch + 1, optimizer.t, sum_loss_epoch / itr_epoch, sum_test_accuracy1 / itr_test, sum_test_accuracy2 / itr_test), iswrite=True, isconsole=False)

        progress = (e + 1) / epoch * 100
        time_end = (datetime.now() - time_start) / (e + 1) * (epoch - e - 1) + datetime.now()

        optimizer.new_epoch()

    serializers.save_npz('model/model_' + name_model, model)
    shutil.copy2('model/model_' + name_model, date + '/model_' + name_model)
    log('Saved the model!', iswrite=False, isconsole=True)

    serializers.save_npz('model/state_' + name_model, optimizer)
    shutil.copy2('model/state_' + name_model, date + '/state_' + name_model)
    log('Saved the optimizer!', iswrite=False, isconsole=True)

    log('{} was required!'.format((datetime.now() - time_start)), iswrite=True, isconsole=True)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('data_train', type=str)
    parser.add_argument('data_test', type=str)
    parser.add_argument('--model', '-m', type=str, default='resnet')
    parser.add_argument('--model_init', '-mi', default='')
    parser.add_argument('--state_init', '-si', default='')
    parser.add_argument('--epoch', '-e', type=int, default=1)
    parser.add_argument('--batchsize', '-b', type=int, default=32)
    parser.add_argument('--batchsize_test', '-bt', type=int, default=512)
    parser.add_argument('--block', '-bl', type=int, default=5)
    parser.add_argument('--rate_learning', '-rl', type=float, default=0.01)
    parser.add_argument('--interval_eval', '-i', type=int, default=1000)
    args = parser.parse_args()

    trainer(args.data_train, args.data_test, args.model, args.model_init, args.state_init, args.epoch, args.batchsize, args.batchsize_test, args.block, args.rate_learning, args.interval_eval)
