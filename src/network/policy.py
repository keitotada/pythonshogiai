from chainer import Chain
import chainer.functions as funcs
import chainer.links as links

from src import common

ch = 192
fcl = 256


class Network(Chain):
    def __init__(self):
        super(Network, self).__init__()
        with self.init_scope():
            self.l1 = links.Convolution2D(in_channels=104, out_channels=ch, ksize=3, pad=1)

            self.l2 = links.Convolution2D(in_channels=ch, out_channels=ch, ksize=3, pad=1)
            self.l3 = links.Convolution2D(in_channels=ch, out_channels=ch, ksize=3, pad=1)
            self.l4 = links.Convolution2D(in_channels=ch, out_channels=ch, ksize=3, pad=1)
            self.l5 = links.Convolution2D(in_channels=ch, out_channels=ch, ksize=3, pad=1)
            self.l6 = links.Convolution2D(in_channels=ch, out_channels=ch, ksize=3, pad=1)
            self.l7 = links.Convolution2D(in_channels=ch, out_channels=ch, ksize=3, pad=1)
            self.l8 = links.Convolution2D(in_channels=ch, out_channels=ch, ksize=3, pad=1)
            self.l9 = links.Convolution2D(in_channels=ch, out_channels=ch, ksize=3, pad=1)
            self.l10 = links.Convolution2D(in_channels=ch, out_channels=ch, ksize=3, pad=1)
            self.l11 = links.Convolution2D(in_channels=ch, out_channels=ch, ksize=3, pad=1)
            self.l12 = links.Convolution2D(in_channels=ch, out_channels=ch, ksize=3, pad=1)

            self.l13 = links.Convolution2D(in_channels=ch, out_channels=common.MOVE_DIRECTION_LABEL_NUM, ksize=1, nobias=True)

            self.l13_bias = links.Bias(shape=9*9*common.MOVE_DIRECTION_LABEL_NUM)


    def __call__(self, x):
        h1 = funcs.relu(self.l1(x))
        h2 = funcs.relu(self.l2(h1))
        h3 = funcs.relu(self.l3(h2))
        h4 = funcs.relu(self.l4(h3))
        h5 = funcs.relu(self.l5(h4))
        h6 = funcs.relu(self.l6(h5))
        h7 = funcs.relu(self.l7(h6))
        h8 = funcs.relu(self.l8(h7))
        h9 = funcs.relu(self.l9(h8))
        h10 = funcs.relu(self.l10(h9))
        h11 = funcs.relu(self.l11(h10))
        h12 = funcs.relu(self.l12(h11))
        h13 = self.l13(h12)
        return self.l13_bias(funcs.reshape(h13, (-1, 9*9*common.MOVE_DIRECTION_LABEL_NUM)))