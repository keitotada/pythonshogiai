from chainer import Chain
import chainer.functions as funcs
import chainer.links as links

from src import common

ch = 192
fcl = 256


class Block(Chain):

    def __init__(self):
        super(Block, self).__init__()
        with self.init_scope():
            self.conv1 = links.Convolution2D(in_channels=ch, out_channels=ch, ksize=3, pad=1, nobias=True)
            self.bn1 = links.BatchNormalization(ch)
            self.conv2 = links.Convolution2D(in_channels=ch, out_channels=ch, ksize=3, pad=1, nobias=True)
            self.bn2 = links.BatchNormalization(ch)

    def __call__(self, x):
        h1 = funcs.relu(self.bn1(self.conv1(x)))
        h2 = self.bn2(self.conv2(h1))
        return funcs.relu(x + h2)


class Network(Chain):

    def __init__(self, blocks):
        super(Network, self).__init__()
        self.blocks = blocks

        with self.init_scope():
            self.lf = links.Convolution2D(in_channels=104, out_channels=ch, ksize=3, pad=1)

            for i in range(1, self.blocks):
                self.add_link('b{}'.format(i), Block())

            self.lpolicy = links.Convolution2D(in_channels=ch, out_channels=common.MOVE_DIRECTION_LABEL_NUM, ksize=1, nobias=True)
            self.lpolicy_bias = links.Bias(shape=(9*9*common.MOVE_DIRECTION_LABEL_NUM))

            self.lvalue1 = links.Convolution2D(in_channels=ch, out_channels=common.MOVE_DIRECTION_LABEL_NUM, ksize=1)
            self.lvalue2 = links.Linear(9*9*common.MOVE_DIRECTION_LABEL_NUM, fcl)
            self.lvalue3 = links.Linear(fcl, 1)

    def __call__(self, x):

        h = funcs.relu(self.lf(x))
        for i in range(1, self.blocks):
            h = self['b{}'.format(i)](h)

        hpolicy = self.lpolicy(h)
        policy = self.lpolicy_bias(funcs.reshape(hpolicy, (-1, 9*9*common.MOVE_DIRECTION_LABEL_NUM)))

        h_value = funcs.relu(self.lvalue1(h))
        h_value = funcs.relu(self.lvalue2(h_value))
        value = self.lvalue3(h_value)
        return policy, value