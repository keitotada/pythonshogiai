from chainer import Chain
import chainer
import chainer.functions as funcs
import chainer.links as links

from src import common

ch = 192
fcl = 256


class Block(Chain):

    def __init__(self, in_ch, rate_growth, layer_n):

        self.layer_n = layer_n
        super(Block, self).__init__()
        with self.init_scope():

            for i in range(layer_n):

                self.add_link('bn{}'.format(i + 1), links.BatchNormalization(in_ch + i * rate_growth))
                self.add_link('conv{}'.format(i + 1), links.Convolution2D(in_channels=in_ch + i * rate_growth, out_channels=rate_growth, ksize=3, stride=1, pad=1))

    def __call__(self, x):

        for i in range(self.layer_n):

            h = funcs.relu(self['bn{}'.format(i + 1)](x))
            x = funcs.concat((x, self['conv{}'.format(i + 1)](h)))

        return x


class Layer_Transition(Chain):

    def __init__(self, in_ch):

        super(Layer_Transition, self).__init__()
        with self.init_scope():

            self.bn = links.BatchNormalization(in_ch)
            self.conv = links.Convolution2D(in_channels=in_ch, out_channels=in_ch, ksize=1)

    def __call__(self, x):

        h = funcs.relu(self.bn(x))
        h = funcs.average_pooling_2d(self.conv(h), ksize=2, stride=2)
        return h


class Network(Chain):

    def __init__(self, layer_n=12, rate_growth=12, block=3):
        self.block = block
        in_chs = [ch + layer_n * rate_growth * i for i in range(self.block + 1)]
        print(in_chs[self.block])
        sizefilter = -(common.MOVE_DIRECTION_LABEL_NUM - in_chs[self.block] - 1)
        super(Network, self).__init__()

        with self.init_scope():
            self.conv1 = links.Convolution2D(in_channels=104, out_channels=ch, ksize=3, stride=1, pad=1)
            for i in range(self.block - 1):
                self.add_link('block{}'.format(i + 2), Block(in_chs[i], rate_growth, layer_n))
                self.add_link('trans{}'.format(i + 2), Layer_Transition(in_chs[i + 1]))

            self.add_link('block{}'.format(self.block + 1), Block(in_chs[self.block - 1], rate_growth, layer_n))
            self.bn = links.BatchNormalization(in_chs[self.block])
            #self.fcl = links.Linear(in_chs[self.block], common.MOVE_DIRECTION_LABEL_NUM)

            self.lpolicy = links.Convolution2D(in_channels=in_chs[self.block], out_channels=common.MOVE_DIRECTION_LABEL_NUM, ksize=1,
                                               nobias=True)
            self.lpolicy_bias = links.Bias(shape=(9 * 9 * common.MOVE_DIRECTION_LABEL_NUM))

            '''self.lvalue1 = links.Convolution2D(in_channels=in_chs[self.block], out_channels=common.MOVE_DIRECTION_LABEL_NUM, ksize=1)
            self.lvalue2 = links.Linear(9 * 9 * common.MOVE_DIRECTION_LABEL_NUM, fcl)
            self.lvalue3 = links.Linear(fcl, 1)'''

    def __call__(self, x):

        h = self.conv1(x)
        for i in range(self.block - 1):
            h = self['block{}'.format(i + 2)](h)
            h = self['trans{}'.format(i + 2)](h)

        h = self['block{}'.format(self.block + 1)](h)
        h = funcs.relu(self.bn(h))
        h = funcs.average_pooling_2d(h, h.data.shape[2])
        #h = self.fcl(h)

        hpolicy = self.lpolicy(h)
        policy = self.lpolicy_bias(funcs.reshape(hpolicy, (-1, 9*9*common.MOVE_DIRECTION_LABEL_NUM)))

        '''h_value = funcs.relu(self.lvalue1(h))
        h_value = funcs.relu(self.lvalue2(h_value))
        value = self.lvalue3(h_value)'''

        return policy, policy


