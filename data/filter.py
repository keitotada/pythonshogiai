import argparse, os, random, statistics, re

ratio = 10
len_min = 50
rate_min = 2500

parser = argparse.ArgumentParser()
parser.add_argument('dir', type=str)
parser.add_argument('output', type=str)
parser.add_argument('--size', '-s', default=1000000, type=int)
args = parser.parse_args()


def make_data(_dir, _output, _size):
    path_kifu = []
    for _cur, _dirs, _files in os.walk(_dir):
        for _file in _files:
            _name, _ext = os.path.splitext(_file)
            if _ext == '.csa':
                path_kifu.append(os.path.join(_cur, _file))

    random.shuffle(path_kifu)
    pattern = re.compile(r"^'(black|white)_rate:.*:(.*)$")
    count = 0
    total_train = 0
    total_test = 0
    total_lost = 0
    for _path in path_kifu:
        _rate = {}
        _len = 0
        _toryo = False
        _file = open(_path, 'r', encoding='utf-8')
        for _line in _file:
            _line = _line.strip()
            _match = pattern.match(_line)
            if _match:
                _rate[_match.group(1)] = float(_match.group(2))

            if _line[:1] == '+' or _line[:1] == '-':
                _len += 1

            if _line == '%TORYO':
                _toryo = True

        _file.close()

        if _toryo and _len >= len_min and len(_rate) > 0 and min(_rate.values()) >= rate_min:
            if count < ratio:
                count += 1
                total_train += 1
                with open(_output + '_train.txt', 'src') as _file:
                    _file.write(_path)
                    _file.write('\n')

                if total_train >= _size:
                    break

            else:
                count = 0
                total_test += 1
                with open(_output + '_test.txt', 'src') as _file:
                    _file.write(_path)
                    _file.write('\n')

        else:
            total_lost += 1

    print('Finished generating {} kifu lists!!'.format(total_train + total_test))
    print('train : {}'.format(total_train))
    print('test : {}'.format(total_test))
    print('lost: {}'.format(total_lost))


if __name__ == "__main__":
    make_data(args.dir, args.output, args.size)