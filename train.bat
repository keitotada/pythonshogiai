@echo off
python train_resnet.py data/kifu_2017_10000_train.txt data/kifu_2017_10000_test.txt -e 5 -m resnet_block25 -bl 25 -rl 0.1
python train_resnet.py data/kifu_2017_10000_train.txt data/kifu_2017_10000_test.txt -e 5 -m resnet_block25 -bl 25 -rl 0.1 -mi model/model_resnet_block25 -si model/state_resnet_block25
python train_resnet.py data/kifu_2017_10000_train.txt data/kifu_2017_10000_test.txt -e 5 -m resnet_block25 -bl 25 -rl 0.01  -mi model/model_resnet_block25 -si model/state_resnet_block25
python train_resnet.py data/kifu_2017_10000_train.txt data/kifu_2017_10000_test.txt -e 5 -m resnet_block25 -bl 25 -rl 0.001  -mi model/model_resnet_block25 -si model/state_resnet_block25

