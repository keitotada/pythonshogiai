resnet_block10
Training Data: data/kifu_2017_10000_train.txt
Testing Data: data/kifu_2017_10000_test.txt
Resnet Block: 10
Epoch: 5
Training Batchsize: 32
Testing Batchsize: 512
Rate of learning: 0.1
Interval Eval: 1000
epoch: 1, iter: 1000, loss: 6.5612154, accur: 0.16015625, 0.5371094
epoch: 1, iter: 2000, loss: 5.0127254, accur: 0.21484375, 0.6347656
epoch: 1, iter: 3000, loss: 4.569445, accur: 0.21679688, 0.62109375
epoch: 1, iter: 4000, loss: 4.3013353, accur: 0.25195312, 0.6074219
epoch: 1, iter: 5000, loss: 4.123451, accur: 0.234375, 0.62890625
epoch: 1, iter: 6000, loss: 4.0005407, accur: 0.26171875, 0.5859375
epoch: 1, iter: 7000, loss: 3.851214, accur: 0.27539062, 0.62890625
epoch: 1, iter: 8000, loss: 3.7856162, accur: 0.26367188, 0.625
epoch: 1, iter: 9000, loss: 3.6895528, accur: 0.2578125, 0.6191406
epoch: 1, iter: 10000, loss: 3.639649, accur: 0.27734375, 0.6621094
epoch: 1, iter: 11000, loss: 3.574302, accur: 0.26367188, 0.6425781
epoch: 1, iter: 12000, loss: 3.5420635, accur: 0.24414062, 0.6484375
epoch: 1, iter: 13000, loss: 3.4811788, accur: 0.29296875, 0.6484375
epoch: 1, iter: 14000, loss: 3.4395726, accur: 0.30664062, 0.6328125
epoch: 1, iter: 15000, loss: 3.3997855, accur: 0.27734375, 0.66015625
epoch: 1, iter: 16000, loss: 3.3718188, accur: 0.30859375, 0.63671875
epoch: 1, iter: 17000, loss: 3.3380203, accur: 0.28515625, 0.6386719
epoch: 1, iter: 18000, loss: 3.3156137, accur: 0.28710938, 0.60546875
epoch: 1, iter: 19000, loss: 3.2696626, accur: 0.296875, 0.6582031
epoch: 1, iter: 20000, loss: 3.2535577, accur: 0.29492188, 0.6328125
epoch: 1, iter: 21000, loss: 3.2588685, accur: 0.34179688, 0.6933594
epoch: 1, iter: 22000, loss: 3.2199764, accur: 0.32226562, 0.6582031
epoch: 1, iter: 23000, loss: 3.198388, accur: 0.33984375, 0.6640625
epoch: 1, iter: 24000, loss: 3.1669, accur: 0.3359375, 0.64453125
epoch: 1, iter: 25000, loss: 3.1743858, accur: 0.30273438, 0.671875
epoch: 1, iter: 26000, loss: 3.1438231, accur: 0.34179688, 0.6484375
epoch: 1, iter: 27000, loss: 3.1214707, accur: 0.31835938, 0.6582031
epoch: 1, iter: 28000, loss: 3.1087573, accur: 0.27929688, 0.65625
epoch: 1, iter: 29000, loss: 3.1034482, accur: 0.328125, 0.6796875
epoch: 1, iter: 30000, loss: 3.0729017, accur: 0.3359375, 0.69140625
epoch: 1, iter: 31000, loss: 3.076435, accur: 0.35351562, 0.6386719
epoch: 1, iter: 32000, loss: 3.0769417, accur: 0.3359375, 0.6386719
epoch: 1, iter: 33000, loss: 3.0338762, accur: 0.31445312, 0.640625
epoch: 1, iter: 34000, loss: 3.0232828, accur: 0.33984375, 0.6738281
epoch: 1, iter: 35000, loss: 3.0086122, accur: 0.38671875, 0.6621094
epoch: 1, iter: 36000, loss: 3.0170522, accur: 0.3203125, 0.6953125
epoch: 1, iter: 37000, loss: 3.0077536, accur: 0.328125, 0.6640625
epoch: 1, iter: 38000, loss: 2.9817948, accur: 0.3515625, 0.671875
epoch: 1, iter: 39000, loss: 2.9608955, accur: 0.31445312, 0.6191406
epoch: 1, iter: 40000, loss: 2.961568, accur: 0.359375, 0.66796875
epoch: 1, iter: 41000, loss: 2.9779322, accur: 0.34375, 0.6875
epoch: 1, iter: 42000, loss: 2.9432042, accur: 0.34960938, 0.7050781
epoch: 1, iter: 42370, loss: 3.4518414, accur: 0.33536497, 0.6719784
epoch: 2, iter: 43000, loss: 2.9100747, accur: 0.3671875, 0.6894531
epoch: 2, iter: 44000, loss: 2.8752873, accur: 0.32226562, 0.65625
epoch: 2, iter: 45000, loss: 2.8472245, accur: 0.34960938, 0.6425781
epoch: 2, iter: 46000, loss: 2.8554595, accur: 0.35546875, 0.7128906
epoch: 2, iter: 47000, loss: 2.8405828, accur: 0.35546875, 0.6621094
epoch: 2, iter: 48000, loss: 2.8264685, accur: 0.375, 0.67578125
epoch: 2, iter: 49000, loss: 2.8326118, accur: 0.38671875, 0.6328125
epoch: 2, iter: 50000, loss: 2.8293724, accur: 0.3203125, 0.66796875
epoch: 2, iter: 51000, loss: 2.8306713, accur: 0.36914062, 0.6347656
epoch: 2, iter: 52000, loss: 2.8128788, accur: 0.33984375, 0.7050781
epoch: 2, iter: 53000, loss: 2.7972648, accur: 0.35546875, 0.66796875
epoch: 2, iter: 54000, loss: 2.8096986, accur: 0.36914062, 0.6699219
epoch: 2, iter: 55000, loss: 2.7952466, accur: 0.37695312, 0.6933594
epoch: 2, iter: 56000, loss: 2.787404, accur: 0.39453125, 0.6425781
epoch: 2, iter: 57000, loss: 2.7906559, accur: 0.33984375, 0.6699219
epoch: 2, iter: 58000, loss: 2.7833421, accur: 0.35546875, 0.6972656
epoch: 2, iter: 59000, loss: 2.7766593, accur: 0.34960938, 0.7128906
epoch: 2, iter: 60000, loss: 2.7843847, accur: 0.33007812, 0.6953125
epoch: 2, iter: 61000, loss: 2.7746012, accur: 0.33398438, 0.6953125
epoch: 2, iter: 62000, loss: 2.7647126, accur: 0.3515625, 0.65234375
epoch: 2, iter: 63000, loss: 2.7558658, accur: 0.37304688, 0.6933594
epoch: 2, iter: 64000, loss: 2.76553, accur: 0.34960938, 0.70703125
epoch: 2, iter: 65000, loss: 2.7461138, accur: 0.36328125, 0.69140625
epoch: 2, iter: 66000, loss: 2.7475395, accur: 0.36523438, 0.6855469
epoch: 2, iter: 67000, loss: 2.7403724, accur: 0.41015625, 0.671875
epoch: 2, iter: 68000, loss: 2.7162476, accur: 0.35351562, 0.69921875
epoch: 2, iter: 69000, loss: 2.7249453, accur: 0.38671875, 0.66796875
epoch: 2, iter: 70000, loss: 2.7054086, accur: 0.3984375, 0.6875
epoch: 2, iter: 71000, loss: 2.7152426, accur: 0.32617188, 0.67578125
epoch: 2, iter: 72000, loss: 2.7050235, accur: 0.39453125, 0.6777344
epoch: 2, iter: 73000, loss: 2.6919014, accur: 0.3515625, 0.6875
epoch: 2, iter: 74000, loss: 2.688005, accur: 0.40234375, 0.6855469
epoch: 2, iter: 75000, loss: 2.6762612, accur: 0.3828125, 0.6796875
epoch: 2, iter: 76000, loss: 2.6676297, accur: 0.3828125, 0.6894531
epoch: 2, iter: 77000, loss: 2.6761494, accur: 0.36132812, 0.6972656
epoch: 2, iter: 78000, loss: 2.6722178, accur: 0.38671875, 0.6542969
epoch: 2, iter: 79000, loss: 2.6517441, accur: 0.41796875, 0.6855469
epoch: 2, iter: 80000, loss: 2.6519556, accur: 0.3671875, 0.6640625
epoch: 2, iter: 81000, loss: 2.66125, accur: 0.32617188, 0.66796875
epoch: 2, iter: 82000, loss: 2.6565397, accur: 0.36523438, 0.69921875
epoch: 2, iter: 83000, loss: 2.6398516, accur: 0.34179688, 0.6699219
epoch: 2, iter: 84000, loss: 2.619454, accur: 0.3828125, 0.6777344
epoch: 2, iter: 84740, loss: 2.7484958, accur: 0.35652333, 0.67106974
epoch: 3, iter: 85000, loss: 2.6023638, accur: 0.34570312, 0.69140625
epoch: 3, iter: 86000, loss: 2.539026, accur: 0.40625, 0.6972656
epoch: 3, iter: 87000, loss: 2.525702, accur: 0.40429688, 0.66796875
epoch: 3, iter: 88000, loss: 2.5351007, accur: 0.33007812, 0.6953125
epoch: 3, iter: 89000, loss: 2.538928, accur: 0.3984375, 0.6796875
epoch: 3, iter: 90000, loss: 2.545032, accur: 0.39648438, 0.6621094
epoch: 3, iter: 91000, loss: 2.5415936, accur: 0.38671875, 0.6660156
epoch: 3, iter: 92000, loss: 2.5349436, accur: 0.40039062, 0.65234375
epoch: 3, iter: 93000, loss: 2.5282311, accur: 0.41015625, 0.6308594
epoch: 3, iter: 94000, loss: 2.5345166, accur: 0.39257812, 0.6855469
epoch: 3, iter: 95000, loss: 2.5249789, accur: 0.37109375, 0.6875
epoch: 3, iter: 96000, loss: 2.5170498, accur: 0.35546875, 0.6386719
epoch: 3, iter: 97000, loss: 2.5318265, accur: 0.37890625, 0.6953125
epoch: 3, iter: 98000, loss: 2.519873, accur: 0.43554688, 0.671875
epoch: 3, iter: 99000, loss: 2.5015638, accur: 0.3671875, 0.66015625
epoch: 3, iter: 100000, loss: 2.527364, accur: 0.3515625, 0.6347656
epoch: 3, iter: 101000, loss: 2.5174623, accur: 0.40234375, 0.6777344
epoch: 3, iter: 102000, loss: 2.5040033, accur: 0.4140625, 0.66796875
epoch: 3, iter: 103000, loss: 2.5118852, accur: 0.375, 0.66796875
epoch: 3, iter: 104000, loss: 2.517464, accur: 0.38671875, 0.6875
epoch: 3, iter: 105000, loss: 2.4931366, accur: 0.36523438, 0.6660156
epoch: 3, iter: 106000, loss: 2.5091672, accur: 0.40234375, 0.6621094
epoch: 3, iter: 107000, loss: 2.4920716, accur: 0.3984375, 0.6855469
epoch: 3, iter: 108000, loss: 2.4949763, accur: 0.38085938, 0.6738281
epoch: 3, iter: 109000, loss: 2.4837637, accur: 0.41210938, 0.6269531
epoch: 3, iter: 110000, loss: 2.484121, accur: 0.38085938, 0.66796875
epoch: 3, iter: 111000, loss: 2.487979, accur: 0.34570312, 0.6699219
epoch: 3, iter: 112000, loss: 2.4836254, accur: 0.38085938, 0.6894531
epoch: 3, iter: 113000, loss: 2.482481, accur: 0.38867188, 0.7011719
epoch: 3, iter: 114000, loss: 2.4760933, accur: 0.36523438, 0.6972656
epoch: 3, iter: 115000, loss: 2.4868755, accur: 0.38085938, 0.6660156
epoch: 3, iter: 116000, loss: 2.4814157, accur: 0.41210938, 0.70703125
epoch: 3, iter: 117000, loss: 2.4811158, accur: 0.3671875, 0.7050781
epoch: 3, iter: 118000, loss: 2.460169, accur: 0.39648438, 0.671875
epoch: 3, iter: 119000, loss: 2.4654849, accur: 0.35546875, 0.69921875
epoch: 3, iter: 120000, loss: 2.4545288, accur: 0.41601562, 0.6875
epoch: 3, iter: 121000, loss: 2.450284, accur: 0.41210938, 0.6894531
epoch: 3, iter: 122000, loss: 2.438172, accur: 0.3828125, 0.6621094
epoch: 3, iter: 123000, loss: 2.454764, accur: 0.37109375, 0.6777344
epoch: 3, iter: 124000, loss: 2.4529943, accur: 0.41210938, 0.6796875
epoch: 3, iter: 125000, loss: 2.4345164, accur: 0.37304688, 0.6542969
epoch: 3, iter: 126000, loss: 2.4496646, accur: 0.40039062, 0.703125
epoch: 3, iter: 127000, loss: 2.444053, accur: 0.38476562, 0.6816406
epoch: 3, iter: 127110, loss: 2.4959333, accur: 0.37363327, 0.6756575
epoch: 4, iter: 128000, loss: 2.3384714, accur: 0.39453125, 0.6796875
epoch: 4, iter: 129000, loss: 2.329719, accur: 0.40234375, 0.671875
epoch: 4, iter: 130000, loss: 2.334757, accur: 0.40429688, 0.6484375
epoch: 4, iter: 131000, loss: 2.3422327, accur: 0.39257812, 0.66796875
epoch: 4, iter: 132000, loss: 2.3234262, accur: 0.40820312, 0.65625
epoch: 4, iter: 133000, loss: 2.3491623, accur: 0.3984375, 0.7050781
epoch: 4, iter: 134000, loss: 2.33132, accur: 0.4296875, 0.6953125
epoch: 4, iter: 135000, loss: 2.3298273, accur: 0.38476562, 0.6953125
epoch: 4, iter: 136000, loss: 2.347728, accur: 0.3984375, 0.6425781
epoch: 4, iter: 137000, loss: 2.3333564, accur: 0.40039062, 0.7109375
epoch: 4, iter: 138000, loss: 2.3409636, accur: 0.38867188, 0.6503906
epoch: 4, iter: 139000, loss: 2.3396728, accur: 0.3828125, 0.69921875
epoch: 4, iter: 140000, loss: 2.3336763, accur: 0.38671875, 0.69140625
epoch: 4, iter: 141000, loss: 2.3247178, accur: 0.40625, 0.69921875
epoch: 4, iter: 142000, loss: 2.3384473, accur: 0.390625, 0.6894531
epoch: 4, iter: 143000, loss: 2.3484805, accur: 0.43945312, 0.6542969
epoch: 4, iter: 144000, loss: 2.3357255, accur: 0.37109375, 0.6542969
epoch: 4, iter: 145000, loss: 2.3410025, accur: 0.375, 0.6484375
epoch: 4, iter: 146000, loss: 2.3428173, accur: 0.38867188, 0.6542969
epoch: 4, iter: 147000, loss: 2.3302882, accur: 0.43164062, 0.6464844
epoch: 4, iter: 148000, loss: 2.3456895, accur: 0.4140625, 0.70703125
epoch: 4, iter: 149000, loss: 2.3354158, accur: 0.34765625, 0.6816406
epoch: 4, iter: 150000, loss: 2.3269293, accur: 0.39453125, 0.69921875
epoch: 4, iter: 151000, loss: 2.329598, accur: 0.39453125, 0.69140625
epoch: 4, iter: 152000, loss: 2.3348927, accur: 0.38085938, 0.6738281
epoch: 4, iter: 153000, loss: 2.3325238, accur: 0.41601562, 0.6484375
epoch: 4, iter: 154000, loss: 2.340596, accur: 0.41601562, 0.67578125
epoch: 4, iter: 155000, loss: 2.3258882, accur: 0.39648438, 0.6894531
epoch: 4, iter: 156000, loss: 2.3256822, accur: 0.39453125, 0.6972656
epoch: 4, iter: 157000, loss: 2.3134563, accur: 0.40625, 0.7167969
epoch: 4, iter: 158000, loss: 2.3052857, accur: 0.41992188, 0.66796875
epoch: 4, iter: 159000, loss: 2.311692, accur: 0.40039062, 0.6855469
epoch: 4, iter: 160000, loss: 2.324929, accur: 0.41210938, 0.6425781
epoch: 4, iter: 161000, loss: 2.3253875, accur: 0.3984375, 0.67578125
epoch: 4, iter: 162000, loss: 2.314525, accur: 0.39257812, 0.6894531
epoch: 4, iter: 163000, loss: 2.298003, accur: 0.39648438, 0.66796875
epoch: 4, iter: 164000, loss: 2.3165646, accur: 0.375, 0.6855469
epoch: 4, iter: 165000, loss: 2.310445, accur: 0.40625, 0.65234375
epoch: 4, iter: 166000, loss: 2.3286777, accur: 0.3984375, 0.6972656
epoch: 4, iter: 167000, loss: 2.3016255, accur: 0.43554688, 0.6796875
epoch: 4, iter: 168000, loss: 2.3166614, accur: 0.41210938, 0.63671875
epoch: 4, iter: 169000, loss: 2.3174307, accur: 0.42578125, 0.69140625
epoch: 4, iter: 169480, loss: 2.3287451, accur: 0.38836437, 0.6712692
epoch: 5, iter: 170000, loss: 2.2500036, accur: 0.37695312, 0.6621094
epoch: 5, iter: 171000, loss: 2.184649, accur: 0.42382812, 0.6660156
epoch: 5, iter: 172000, loss: 2.1888418, accur: 0.3984375, 0.6640625
epoch: 5, iter: 173000, loss: 2.1920865, accur: 0.40625, 0.65625
epoch: 5, iter: 174000, loss: 2.1949496, accur: 0.41210938, 0.6738281
epoch: 5, iter: 175000, loss: 2.1953585, accur: 0.3984375, 0.67578125
epoch: 5, iter: 176000, loss: 2.2008593, accur: 0.37890625, 0.6816406
epoch: 5, iter: 177000, loss: 2.2139492, accur: 0.37304688, 0.6953125
epoch: 5, iter: 178000, loss: 2.1947935, accur: 0.390625, 0.6796875
epoch: 5, iter: 179000, loss: 2.207764, accur: 0.4296875, 0.7011719
epoch: 5, iter: 180000, loss: 2.2187293, accur: 0.41015625, 0.69140625
epoch: 5, iter: 181000, loss: 2.2073536, accur: 0.43945312, 0.7109375
epoch: 5, iter: 182000, loss: 2.2070992, accur: 0.37695312, 0.6660156
epoch: 5, iter: 183000, loss: 2.2223642, accur: 0.41796875, 0.6640625
epoch: 5, iter: 184000, loss: 2.214194, accur: 0.38085938, 0.6542969
epoch: 5, iter: 185000, loss: 2.2219088, accur: 0.40039062, 0.6347656
epoch: 5, iter: 186000, loss: 2.2126417, accur: 0.43164062, 0.6933594
epoch: 5, iter: 187000, loss: 2.207157, accur: 0.41992188, 0.71875
epoch: 5, iter: 188000, loss: 2.2210784, accur: 0.4140625, 0.6875
epoch: 5, iter: 189000, loss: 2.2300882, accur: 0.39648438, 0.6972656
epoch: 5, iter: 190000, loss: 2.2184186, accur: 0.4140625, 0.6738281
epoch: 5, iter: 191000, loss: 2.228161, accur: 0.43945312, 0.6875
epoch: 5, iter: 192000, loss: 2.2117112, accur: 0.40234375, 0.671875
epoch: 5, iter: 193000, loss: 2.21664, accur: 0.41992188, 0.6972656
epoch: 5, iter: 194000, loss: 2.1941278, accur: 0.35351562, 0.6542969
epoch: 5, iter: 195000, loss: 2.2131145, accur: 0.375, 0.68359375
epoch: 5, iter: 196000, loss: 2.2009263, accur: 0.3984375, 0.6582031
epoch: 5, iter: 197000, loss: 2.2179387, accur: 0.35351562, 0.6796875
epoch: 5, iter: 198000, loss: 2.2113562, accur: 0.390625, 0.6875
epoch: 5, iter: 199000, loss: 2.221689, accur: 0.453125, 0.6953125
epoch: 5, iter: 200000, loss: 2.2163258, accur: 0.37695312, 0.671875
epoch: 5, iter: 201000, loss: 2.202462, accur: 0.3671875, 0.6621094
epoch: 5, iter: 202000, loss: 2.2210116, accur: 0.40039062, 0.6464844
epoch: 5, iter: 203000, loss: 2.1971874, accur: 0.3984375, 0.63671875
epoch: 5, iter: 204000, loss: 2.2078636, accur: 0.42578125, 0.6738281
epoch: 5, iter: 205000, loss: 2.2109985, accur: 0.43164062, 0.7011719
epoch: 5, iter: 206000, loss: 2.21286, accur: 0.43164062, 0.6660156
epoch: 5, iter: 207000, loss: 2.2071645, accur: 0.40234375, 0.66796875
epoch: 5, iter: 208000, loss: 2.2037888, accur: 0.40429688, 0.6855469
epoch: 5, iter: 209000, loss: 2.1962292, accur: 0.421875, 0.6855469
epoch: 5, iter: 210000, loss: 2.1964605, accur: 0.421875, 0.6582031
epoch: 5, iter: 211000, loss: 2.2061605, accur: 0.375, 0.6894531
epoch: 5, iter: 211850, loss: 2.2083685, accur: 0.392302, 0.6743794
4:25:12.471313 was required!
