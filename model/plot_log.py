﻿import argparse, re, matplotlib.pyplot as plt

def plot_log(dir):
    data = re.compile(r'iter: ([0-9]+), loss: ([0-9.]+), accur: ([0-9.]+)')
    ite_list = []
    loss_list = []
    accur_list = []
    _file = open(dir + '/log.txt', 'r')
    for _line in _file:
        isdata = data.search(_line)
        if isdata:
            ite_list.append(int(isdata.group(1)))
            loss_list.append(float(isdata.group(2)))
            accur_list.append(float(isdata.group(3)))

    _file.close()

    fig, ax1 = plt.subplots()
    p1, = ax1.plot(ite_list, loss_list, 'b', label='loss')
    ax1.set_xlabel('iterations')

    ax2 = ax1.twinx()
    p2, = ax2.plot(ite_list, accur_list, 'g', label='accuracy')

    ax1.legend(handles=[p1, p2])

    plt.title(dir)
    plt.show()


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('dir', type=str)
    plot_log(parser.parse_args().dir)